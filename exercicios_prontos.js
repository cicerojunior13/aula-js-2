//TODOS OS EXERCICIOS PRONTOS ESTAO EM MODO LEITURA

// Exercicio 1
// gods.forEach(god => console.log(`${god.name} ${god.features.length}`))

// Exercicio 2
// let filtro = gods.filter(god => god.roles.includes("Mid"))
// console.log(filtro)

// Exercicio 3
// function compare(a, b){
//   if (a.pantheon < b.pantheon)
//     return -1;
//   if (a.pantheon > b.pantheon)
//     return 1;
//   return 0;
// }
// gods.sort(compare);
// console.log(gods)

// Exercicio 4
// gods.forEach(god => console.log(`${god.name} (${god.class})`))

// Exercicio 5
// let MID_HUNTER = gods.filter(god => god.class.includes("Hunter") && god.pantheon.includes("Greek") && god.roles.includes("Mid"))
// console.log(MID_HUNTER)